const addToCartButtons = document.querySelectorAll('.add-to-cart');
        
addToCartButtons.forEach(button => {
    button.addEventListener('click', () => {
        alert('Product added to cart.');
        // You can add more logic here to handle the cart functionality.
    });
});